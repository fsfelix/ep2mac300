#include <stdio.h>
#include <stdlib.h>
 
typedef struct cel{
  int lin, col;
  double valor;
  struct cel *proxLin, *proxCol;
} node;
 
typedef node * pointer;
 
typedef struct{
  int m, n, r;
  pointer *linhas, *colunas;
} matrizEsparsa;
 
void adicionaNaMatriz(matrizEsparsa *m, double x, int i, int j);
matrizEsparsa* criaMatrizEsparsa(int m, int n, int r, char* arquivo);
void destroiMatrizEsparsa(matrizEsparsa *m, int n);
void vezesVetor(matrizEsparsa *m, int n, double a[], double res[]);
void gradientesConjugados(matrizEsparsa *A, double b[], double x[], int n);

matrizEsparsa* criaMatrizEsparsa(int m, int n, int r, char* arquivo){
    matrizEsparsa *new;
    int i, j;
    double x;
    new = malloc(sizeof(matrizEsparsa));
    FILE *fp;
 
    fp = fopen(arquivo, "r");
 
    new->linhas = malloc(m*sizeof(node*));
 
    /*    for(i = 0; i < m; i++){
        new->linhas[i] = malloc(sizeof(node));
        }
    */
 
    new->colunas = malloc(n*sizeof(node*));
    /*
    for(i = 0; i < n; i++){
        new->colunas[i] = malloc(sizeof(node));
    }
    */
    for(i = 0; i < m; i++){
      for(j = 0; j < n; j++){
        fscanf(fp, "%lf", &x);
        if(x != 0) adicionaNaMatriz(new, x, i, j);
      }
    }
 
    /*
    while(!feof(fp))
      {
        fscanf(fp, "%lf", &x);
        if(x != 0)
      }
    */
    new->m = m;
    new->n = n;
    new->r = r;
 
    fclose(fp);
 
    return new;
}
 
void adicionaNaMatriz(matrizEsparsa *m, double x, int i, int j){
  node *new, *tmp;
 
  new = malloc(sizeof(node));
  new->valor = x;
  new->lin = i;
  new->col = j;
 
  if(m->linhas[i] == NULL){
    //    m->linhas[i] = malloc(sizeof(node));
    m->linhas[i] = new;
    new = m->linhas[i];
    new->proxCol = NULL;
  }
  else{
 
    for(tmp = m->linhas[i]; tmp->proxCol != NULL; tmp = tmp->proxCol);
    //for(tmp = m->linhas[i]; tmp != NULL; tmp = tmp->proxCol);
    //tmp->proxCol = malloc(sizeof(node));
    
    tmp->proxCol = new;
    new = tmp->proxCol;
    new->proxCol = NULL;
  }
}

void destroiMatrizEsparsa(matrizEsparsa *m, int n){
  node *inicio, *tmp;
  int i;
  
  for(i = 0; i < n; i++){
    inicio = m->linhas[i];
    while(inicio != NULL){
      tmp = inicio->proxCol;
      free(inicio);
      inicio = tmp;
    }
  }
  
  free(m->linhas);
  free(m->colunas);
  free(m);
}

void vezesVetor(matrizEsparsa *m, int n, double a[], double res[]){
  int i;
  node *tmp;
  for(i = 0; i < n; i++){
    res[i] = 0;
    for(tmp = m->linhas[i]; tmp != NULL; tmp = tmp->proxCol){
      res[i] += (tmp->valor)*a[tmp->col];
    }
  }

  for(i = 0; i < n; i++)
    printf("%lf\n", res[i]);
}

void gradientesConjugados(matrizEsparsa *A, double b[], double x[], int n){
  int i = 0;
  double *r, *p;


}

int main(){
  matrizEsparsa *teste;
  char arquivo[2000];
  int n, r, i;
  node *tmp;

  double a[20], res[20];

  scanf("%d %d %s", &n, &r, arquivo);
 
  teste = criaMatrizEsparsa(n, n, r, arquivo);

  vezesVetor(teste, n, a, res);
  
  printf("\n\n");

  for(i = 0; i < n; i++){
    for(tmp = teste->linhas[i]; tmp != NULL; tmp = tmp->proxCol){
      printf("[%d][%d] %lf", tmp->lin, tmp->col, tmp->valor);
    }
    printf("\n");
  }

  destroiMatrizEsparsa(teste, n);
  //  teste->colunas[9]->valor = 2.3;
 
  //  printf("%d %d %d %lf\n", teste->m, teste->n, teste->r, teste->colunas[9]->valor);
  return 0;
}
