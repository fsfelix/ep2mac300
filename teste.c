#include <stdio.h>
#include <stdlib.h>
 
typedef struct cel{
  int lin, col;
  double valor;
  struct cel *proxCol;
} node;
 
typedef node * pointer;
 
typedef struct{
  int m, n, r;
  pointer *linhas, *colunas;
} matrizEsparsa;
 
void adicionaNaMatriz(matrizEsparsa *m, double x, int i, int j);
matrizEsparsa* criaMatrizEsparsa(int m, int n, int r, char* arquivo);
 
 
void destroiMatrizEsparsa(matrizEsparsa *m, int n){
  node *inicio, *tmp;
  int i;
 
  for(i = 0; i < n; i++){
    inicio = m->linhas[i];
    while(inicio != NULL){
      tmp = inicio->proxCol;
      free( inicio);
      inicio = tmp;
    }
  }
}
 
matrizEsparsa* criaMatrizEsparsa(int m, int n, int r, char* arquivo){
    matrizEsparsa *new;
    int i, j;
    double x;
    new = malloc(sizeof(matrizEsparsa));
    FILE *fp;
 
    fp = fopen(arquivo, "r");
 
    new->linhas = malloc(m*sizeof(node*));
 
    new->colunas = malloc(n*sizeof(node*));
 
    for(i = 0; i < m; i++){
      for(j = 0; j < n; j++){
        fscanf(fp, "%lf", &x);
      //  printf("[%d] [%d]  %lf", i, j, x);
       // fflush(stdout);
        if(x != 0) adicionaNaMatriz(new, x, i, j);
      }
      printf("\n");
    }
 
    new->m = m;
    new->n = n;
    new->r = r;
 
    return new;
}
 
void adicionaNaMatriz(matrizEsparsa *m, double x, int i, int j){
  node *new, *tmp, *a;
 
  new = malloc(sizeof(node));
  new->valor = x;
  new->lin = i;
  new->col = j;
 
  if(m->linhas[i] == NULL){
    //  m->linhas[i] = malloc(sizeof(node));
    m->linhas[i] = new;
    new->proxCol = NULL;
  }
  else{
 
    tmp->proxCol = malloc(sizeof(node));
    tmp->proxCol = new;
    new->proxCol = NULL;
  }
}
 
int main(){
  matrizEsparsa *teste;
  char arquivo[2000];
  int n, r, i;
  node *tmp;
 
  scanf("%d %d %s", &n, &r, arquivo);
 
  teste = criaMatrizEsparsa(n, n, r, arquivo);
 
  for(i = 0; i < n; i++){
    for(tmp = teste->linhas[i]; tmp != NULL; tmp = tmp->proxCol){
      printf("%lf ", tmp->valor);
    }
    printf("\n");
  }
 
  destroiMatrizEsparsa(teste,n);
 
  return 0;
}
